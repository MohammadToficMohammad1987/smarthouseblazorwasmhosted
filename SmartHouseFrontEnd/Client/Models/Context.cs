﻿using SmartHouseFrontEnd.Client.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace SmartHouseFrontEnd.Client.Models
{
    public  class Context
    {
        public String userName { get; set; } 
        public string userId { get; set; } 
        public int temperature { get; set; }
        public AppState.LightType light { get; set; }


        public Context() { 
        
        }
        public Context(string _userName,String _userId, AppState.LightType _light, int _temperature)
        {
            light = _light;
            temperature = _temperature;
            userName = _userName;
            userId = _userId;

        }
    }
}

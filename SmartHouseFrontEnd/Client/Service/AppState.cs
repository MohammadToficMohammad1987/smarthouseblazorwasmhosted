﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHouseFrontEnd.Client.Service
{
    public class AppState
    {

        public AppState() {

        }

        public enum LightType
        {
            OFF = 0,
            DIMMED = 1,
            ON = 2
        };

        public String userName { get; set; } = "UserName";
        public string userId { get; set; } = "";
        public LightType light { get; set; } = LightType.OFF;// 0:off , 1:dimmed , 2:on 
        public int temperature { get; set; } = 18;




        public event Action OnChange;

       

        public  void SetLight(LightType _light)
        {
            light = _light;
            NotifyStateChanged();
        }

        public  void SetTemperature(int _temperature)
        {
            temperature = _temperature;
            NotifyStateChanged();
        }

        public void SetUserName(string _userName)
        {
            userName = _userName;
            NotifyStateChanged();
        }

        public void SetUserId(string _userId)
        {
            userId = _userId;
            NotifyStateChanged();
        }

        private  void NotifyStateChanged() => OnChange?.Invoke();
    }
}

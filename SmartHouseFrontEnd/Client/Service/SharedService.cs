﻿using Microsoft.JSInterop;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System;
using SmartHouseFrontEnd.Client.Models;

namespace SmartHouseFrontEnd.Client.Service
{
    public class SharedService
    {
        private readonly IJSRuntime _jsRuntime;
        private readonly HttpClient _httpClient;
        private readonly AppState _appState;


        public SharedService(IJSRuntime jsRuntime,
                             HttpClient httpClient,
                             AppState appState)
        {
            _jsRuntime = jsRuntime;
            _httpClient = httpClient;
            _appState = appState;
        }

        public  void SetLoading(bool loading)
        {
            string[] input = new string[1] { loading ? "true" : "false" };
            var result = _jsRuntime.InvokeAsync<string>("SetLoading", input);
        }


        public async Task UpdateContext()
        {
            SetLoading(true);
            try
            {
                Context ctx = new Context(_appState.userName, _appState.userId,_appState.light, _appState.temperature);
                await _httpClient.PostJsonAsync("https://localhost:44395/api/rest/UpdateContext", ctx);
            }
            finally{
            SetLoading(false);
            }
          
        }


        public  async Task GetContext()
        {
            SetLoading(true);
            try
            {
                  Context ctx = await _httpClient.GetJsonAsync<Context>("https://localhost:44395/api/rest/GetContext");
                 _appState.SetUserName(ctx.userName);
                 _appState.SetUserId(ctx.userId);
                 _appState.SetLight(ctx.light);
                 _appState.SetTemperature(ctx.temperature);
            }
            finally
            {
                SetLoading(false);
            }
        }

        public async Task UpdateThenGetContext()
        {
            await UpdateContext();
            await GetContext();
        }
    }
}
